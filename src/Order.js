import React, { PureComponent } from 'react';

class Order extends PureComponent {
  render() {
    const { data, style } = this.props;
    return (
      <div className='row' style={style}>
        <div>{data.numberOfOrders}</div>
        <div>{data.amount.toFixed(5)}</div>
        <div>{data.quantity.toFixed(5)}</div>
        <div>{data.price}</div>
        {/* {JSON.stringify(this.props.data)} */}
      </div>
    );
  }
}

export default Order;
