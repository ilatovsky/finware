import React, { Component } from 'react';
function retr_dec(num) {
  return (num.split('.')[1] || []).length;
}
const getBar = (amount,total,reversed) => {
  const color = (reversed)?'rgba(255,230,230,0.7)':'rgba(230,255,230,0.7)';
  const ratio = `${(1-amount/total)*100}`;
  return ({background: `linear-gradient(to ${(reversed)?'left':'right'}, rgba(255,255,255,0) 0%, rgba(255,255,255,0) ${ratio}%, ${color} ${ratio}%,${color} 100%)`,backgroundBlendMode:'overlay'})
};

class OrderTableRow extends Component {
  render() {
    const {data} = this.props
    return (
      (this.props.header)
        ? <div className={
            `OrderBook__TableRow OrderBook__TableHeader${(this.props.reversed) ? ' OrderBook__TableRow--reversed' : ''}`
          } style={(this.props.style)?this.props.style:null}>
          <div className='OrderBook__TableCell'><span>{data.numberOfOrders}</span></div>
          <div className='OrderBook__TableCell'><span>{data.amount}</span></div>
          <div className='OrderBook__TableCell'><span>{data.quantity}</span></div>
          <div className={
                `OrderBook__TableCell ${(!this.props.reversed) ? 'OrderBook__TableCell--alignRight' : null} `
              }><span>{data.price}</span></div>
          </div>
        : <div className={
            `OrderBook__TableRow ${(this.props.reversed) ? ' OrderBook__TableRow--reversed' : ''}`
          } style={(this.props.style)?this.props.style:null}>
          <div className='OrderBook__TableCell'><span>{data.numberOfOrders}</span></div>
          <div className='OrderBook__TableCell'><span>{(retr_dec(data.amount.toString())>5)?data.amount.toFixed(5):data.amount}</span></div>
          <div className='OrderBook__TableCell'><span>{(retr_dec(data.quantity.toString())>5)?data.quantity.toFixed(5):data.quantity}</span></div>
          <div className={
                `OrderBook__TableCell grad ${(!this.props.reversed) ? 'OrderBook__TableCell--alignRight' : null} `
              }
            style={getBar(data.amount,this.props.total,this.props.reversed)}
          ><span>{data.price.toFixed(2)}</span></div>
          </div>
    );
  }
}

export default OrderTableRow;

{ /* <div className='OrderBook__TableCell'><span>Count</span></div>
<div className='OrderBook__TableCell'><span>Total</span></div>
<div className='OrderBook__TableCell'><span>Amount</span></div>
<div className={
  `OrderBook__TableCell
  ${(!this.props.reversed) ? 'OrderBook__TableCell--alignRight' : null}
  `
}><span>Price</span></div> */ }
