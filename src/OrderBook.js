import React, { Component } from 'react';
import OrderTable from './OrderTable';

class OrderBook extends Component {
  render() {
    return (
      <div className='OrderBook'>
        <OrderTable header={{ numberOfOrders:'Count', amount:'Total', quantity: 'Amount', price: 'Price'}} data={this.props.data.asks} />
        <OrderTable header={{ numberOfOrders:'Count', amount:'Total', quantity: 'Amount', price: 'Price'}} data={this.props.data.bids} reversed/>
      </div>
    );
  }
}

export default OrderBook;

{ /* <div className='BookHeader'>
  <div className='Inner'>
    <FaChevronDown color={'#808080'} style={{float:'right'}}/>
    <h1>Order Book</h1>

  </div>
</div> */ }
{ /* <div className='BookContent'>
  <div className='Inner'>
    <OrderTable name={'asks'} orders={this.props.data.asks}/>
    <OrderTable name={'bids'} orders={this.props.data.bids}/>
  </div>
</div> */ }
