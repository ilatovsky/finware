import React from 'react';
import ReactDOM from 'react-dom';

import './style.styl';
import OrderBook from './OrderBook';
import Paper from './Paper';
import data from './1.json';

const Index = () => (
  <Paper title={'Order Book'}>
    <OrderBook data={data}/>
  </Paper>
);

ReactDOM.render((<Index/>), document.getElementById('fullwindow'));
