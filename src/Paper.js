import React, { PureComponent } from 'react';
import { FaChevronDown } from 'react-icons/fa';
import { FaInfoCircle } from 'react-icons/fa';
import { FaCogs } from 'react-icons/fa';

class Paper extends PureComponent {

  render() {
    return (
      <div className='Paper'>
        <div className='Paper__Inner'>
          <div className='Paper__Header'>
            <div className='ToolBar'>
              <div className='ToolBar__Part ToolBar__Part--left'>
                <FaChevronDown className='ToolBar__Icon' style={{marginTop:4}}/>
                <h2>{this.props.title}</h2>
              </div>
              <div className='ToolBar__Part ToolBar__Part--right'>
                <FaInfoCircle className='ToolBar__Icon ToolBar__Icon--big'/>
                <FaCogs className='ToolBar__Icon ToolBar__Icon--big'/>
              </div>
            </div>
          </div>
          <div className='Paper__Content'>
            {this.props.children}
          </div>
        </div>
      </div>
    );
  }

}

export default Paper;
