import React, { Component } from 'react';
import VirtualList from 'react-tiny-virtual-list';
import OrderTableRow from './OrderTableRow';

class OrderTable extends Component {
  calculateAmounts = orders => (
    orders.reduce((acc, item) => {
      const { numberOfOrders, price, quantity } = item;
      const prevAmount = (acc[acc.length - 1]) ? acc[acc.length - 1].amount : 0;
      return ([...acc, ...[{
        numberOfOrders, price, quantity: quantity, amount: prevAmount + quantity,
      }]]);
    }, [])
  )

  render() {
    const orders = this.calculateAmounts(this.props.data);
    return (
      <div className='OrderBook__Table'>
        <OrderTableRow data={this.props.header} header reversed={this.props.reversed}/>
        <VirtualList
          className='OrderBook__TableContent'
          overscanCount={10}
          width='100%'
          height={460}
          itemCount={orders.length}
          itemSize={20} // Also supports variable heights (array or function getter)
          renderItem={({ index, style }) => (
            <OrderTableRow key={index} total={orders[orders.length-1].amount} data={orders[index]} style={style} reversed={this.props.reversed}/>
          )}
        />

        {/* <div className='OrderBook__TableContent'>
          {(this.props.data)
            ? this.calculateAmounts(this.props.data).map((order, index) => (
              <OrderTableRow key={index} data={order} reversed={this.props.reversed} />
            ))
            : null }
        </div> */}
      </div>
    );
  }
}

export default OrderTable;
